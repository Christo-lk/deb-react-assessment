import { useState } from "react"

//Components
import Product from "./Product"

//Helpers
import { formatDate } from "./helpers"

// MUI
import { Grid } from "@mui/material"

type Props = {
    quote: any;
    quotes: any[]
    setQuotes: React.Dispatch<React.SetStateAction<any[]>>;
}

const Quote: React.FC<Props> = ({ quote, quotes, setQuotes }) => {
    const [expanded, setExpanded] = useState<Boolean>(false);

    function handleQuoteSelect() {
        const currentQuote = quotes.filter(q => q.quoteID === quote.quoteID)[0]
        const selected = currentQuote.isSelected

        currentQuote.isSelected = selected ? false : true;

        // get position of current item in array of items
        const currentPosition: number = quotes.map(q => q.quoteID).indexOf(currentQuote.quoteID);

        // create new Array to modify
        const splicedArray: any[] = [...quotes];

        // insert updated item into correct position
        splicedArray.splice(currentPosition, 1, currentQuote);

        // update state in parent component
        setQuotes(splicedArray);
    }

    return (
        <div className={`quote-container ${expanded ? "expanded" : ""} ${quote.isSelected ? "selected" : ""}`} >
            <div className="quote-header-container">
                <Grid container columns={20} spacing={2}>
                    <Grid item xs={3}>
                        <p className="quote-info-title">Advisor</p>
                        <p className="text-semi-bold">{quote.adviserName}</p>
                    </Grid>

                    <Grid item xs={3}>
                        <p className="quote-info-title">Created</p>
                        <p className="text-semi-bold">{formatDate(quote.created)}</p>
                    </Grid>

                    <Grid item xs={3}>
                        <p className="quote-info-title">Quote Expires</p>
                        <p className="text-semi-bold">{formatDate(quote.expirationDate)}</p>
                    </Grid>

                    <Grid item xs={3}>
                        <p className="quote-info-title">Products</p>
                        <p className="text-semi-bold">{quote.items.length}</p>
                    </Grid>

                    <Grid item xs={2}>
                        <p className="quote-info-title">Efficiency </p>
                        <p className="text-semi-bold">{quote.efficiency.toFixed(2)}</p>
                    </Grid>

                    <Grid item xs={2}>
                        <p className="quote-info-title">Savings </p>
                        <p className="text-semi-bold">${quote.energyDelta.money.toFixed(2)}</p>
                    </Grid>

                    <Grid item xs={2}>
                        <p className="quote-info-title">Quote Total </p>
                        <p className="text-semi-bold">${quote.investment.toFixed(2)}</p>
                    </Grid>

                    <Grid item xs={2}>
                        <div className="select-quote-button-container">
                            <button onClick={handleQuoteSelect} className="button button-primary">{quote.isSelected ? "De-select" : "Select"}</button>
                        </div>
                    </Grid>
                </Grid>
            </div>
            {expanded &&
                <div className="quote-expanded-container">
                    <p className="text-secondary">Products: </p>
                    {quote.items.map((item: any, index: number) => <Product key={index} product={item} />)}
                </div>
            }
            <button className="button button-secondary" onClick={() => setExpanded(expanded ? false : true)}>{expanded ? "Collapse" : "Expand"}</button>
        </div>
    )

}

export default Quote