import dayjs from "dayjs"

export function formatDate(date: number, format?: string): string { 
    if(format) { 
        return dayjs(date).format(format)
    }

    return dayjs(date).format("MMMM D, YYYY")
}