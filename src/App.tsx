import { useEffect, useState } from "react"
import './App.scss';
import Quote from "./Quote";

//Models
import { Filter } from "./models"

//Helpers
import quotesJSON from "./quotes.json"

const updatedQuotes = quotesJSON.map((q: any) => {
    q.isSelected = false
    return q
});

function App() {
    const [filter, setFilter] = useState<string>("")
    const [quotes, setQuotes] = useState<any[]>(updatedQuotes);

    useEffect(() => {
        const newQuotes = [...quotes]

        switch (filter) {
            case Filter.dateCreated:
                setQuotes(newQuotes.sort((a, b) => new Date(b.created).valueOf() - new Date(a.created).valueOf()))
                break;
            case Filter.expirationDate:
                setQuotes(newQuotes.sort((a, b) => new Date(b.expirationDate).valueOf() - new Date(a.expirationDate).valueOf()))
                break;

            case Filter.quoteTotal:
                setQuotes(newQuotes.sort((a, b) => b.investment - a.investment))
                break

            default:
                setQuotes(quotesJSON)
        }
    }, [filter])

    function isCurrentFilter(button: string) {
        return button === filter
    }

    function submitHandler() {
        if (!validateQuotes()) {
            alert("Please select at least one quote")
        } else {
            alert("Quote submitted")
        }
    }

    function validateQuotes() {
        const selectedQuotes = quotes.filter(q => q.isSelected === true)

        return selectedQuotes.length > 0 
    }

    return (
        <div className="App">
            <div className="quotes-page-header">
                <h2>Quotes:</h2>

                <div className="header-buttons-container">
                    <div className="filter-buttons-container">
                        <p className="text-semi-bold">Filter by: </p>
                        <button onClick={() => setFilter(filter => filter !== Filter.dateCreated ? Filter.dateCreated : "")} className={`button button-secondary ${isCurrentFilter(Filter.dateCreated) && "selected"}`}>Date Created</button>
                        <button onClick={() => setFilter(filter => filter !== Filter.expirationDate ? Filter.expirationDate : "")} className={`button button-secondary ${isCurrentFilter(Filter.expirationDate) && "selected"}`}>Expiration Date</button>
                        <button onClick={() => setFilter(filter => filter !== Filter.quoteTotal ? Filter.quoteTotal : "")} className={`button button-secondary ${isCurrentFilter(Filter.quoteTotal) && "selected"}`}>Quote Total</button>
                    </div>

                    <button onClick={() => submitHandler()} className="button button-action">Submit</button>
                </div>
            </div>

            <p className="">Select which quotes you would like to proceed with: </p>

            <div className="quotes">
                {quotes.map((quote, index) => <Quote key={index} quote={quote} setQuotes={setQuotes} quotes={quotes} />)}
            </div>
        </div>
    )
}

export default App
