// MUI
import { Grid } from "@mui/material"

type Props = {
    product: any
}

const Product: React.FC<Props> = ({ product }) => {
    const productTotal = (product.amount * product.retailPrice).toFixed(2)

    return (
        <div className="product-container">
            <Grid container columns={20} spacing={2}>
                <Grid item xs={3}>
                    <div className="image-container">
                        <img src="https://placem.at/things?w=500" alt="Product-image" />
                    </div>
                </Grid>

                <Grid item xs={3}>
                    <p className="product-info-title">Title</p>
                    <p>{product.product.title}</p>
                </Grid>

                <Grid item xs={6}>
                    <div className="product-description-container">
                        <p className="product-info-title">Description</p>
                        <p>{product.product.description ? product.product.description : "Geen beschrijving beschikbaar."}</p>
                    </div>
                </Grid>

                <Grid item xs={2}>
                    <p className="product-info-title">Units</p>
                    <p className="">{product.amount}</p>
                </Grid>

                <Grid item xs={2}>
                    <p className="product-info-title">Price Per Unit</p>
                    <p className="">${product.retailPrice}</p>
                </Grid>

                <Grid item xs={2}>
                    <p className="product-info-title">Product Total</p>
                    <p className="text-semi-bold">${productTotal}</p>
                </Grid>

                {/* This is here to align the product info grid with the quote info grid */}
                <Grid item xs={2}/>
            </Grid>
        </div>
    )
}

export default Product