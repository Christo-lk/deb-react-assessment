// I created these models but didnt have time to troubleshoot which fields were nullable to implement them across the project properly
export type QuoteType = {
    adviserName: string;
    created: number;
    expirationDate: number;
    investment: number;
    isAccepted: boolean;
    isAdvised: boolean;
    isCancelled: boolean;
    isExpired: boolean;
    isSeen: boolean;
    isSent: boolean;
    reference: string;
    efficiency: number;
    paybackTime: number;
    sentOn: number;
    solution: string;
    subsidy: number;
    items: ProductType[];
    energyDelta: EnergyDeltaType
}

export type EnergyDeltaType = { 
    electricityConsumption: number;
    electricityProduction: number;
    gasConsumption: number;
    gasFactor: number;
    emission: number;
    money: number;
}

export type ProductType = {
    amount: number;
    retailPrice: number;
    product: ProductDataType
}

export type ProductDataType = { 
    advantages?: string[]
    title: string;
    description: string;
    solution: string;
    category: string;
    priceUnit: string;
    tax: number;
    brand: string | null;
    productWarranty: number | null;
    pMax: number
}

export enum Filter {
    dateCreated = "dateCreated",
    expirationDate ="expirationDate",
    quoteTotal = "quoteTotal"
}